# ✨ WelcomeMessage

An easy minecraft plugin for WelcomeMessage!

💻 Versions: 1.20+

📝 Configuration:
```yaml
# WelcomeMessage 1.0
# 2024-present ©️ poldekDEV
#https://gitlab.com/poldekDEV/welcomemessage

#Message on join:
welcome_msg: "&e✨Welcome %player_name% to WelcomeMessage!\n&fTo change that message please check WelcomeMessage/config.yml\n&aWe are supporting &2PlaceholderAPI!"
```
🧩 This plugin supports [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/)

🗒️ You can access ChangeLog [Here](https://gitlab.com/poldekDEV/welcomemessage/-/blob/main/CHANGELOG.MD?ref_type=heads)

🔗 You can download plugin for **free** [here](https://gitlab.com/poldekDEV/welcomemessage/-/releases) *(Should redirect to newest version)*
📊 Wanted to check statistics about our plugin? [Click here](https://bstats.org/plugin/bukkit/poldekDEV-WelcomeMessage/22465) (1.0.1 versions & up)

✅ Need help or wanted to stay up-to-date? Here you can join our discord: https://discord.gg/YPc4bHWhW2

🇵🇱 Developed and maintained in Poland!

*2024 ©️ poldekDEV*



